Case Study: Eye of GNOME and GNOME Session Manager rewrites
===========================================================
Interview Form
--------------

My name is Antonio Terceiro, and I am a Software Engineering PhD student
at `Universidade Federal da Bahia`_, `Computer Science Department`_,
under supervision of `Professor Christina von Flach Garcia Chavez`_.

.. _`Universidade Federal da Bahia`: http://www.ufba.br/
.. _`Computer Science Department`: http://www.dcc.ufba.abr/
.. _`Professor Christina von Flach Garcia Chavez`: http://www.dcc.ufba.br/~flach

I am conducting a case study about the rewrites that were done in two
GNOME modules, namely Eye of GNOME (eog) and GNOME Sesssion Manager
(gnome-session). The goal of this case study is to understand the
problems that lead developers to spend their time rewriting a project
from scratch, when they could be just performing maintenance activities
in the existing code base.

I am trying to trace those problems back to characteristics that can be
automatically extracted from the source code. If that is successful,
then I will be able to propose methods and techniques to help developers
avoid the same kind of problems in their own projects. This could
benefit other free software projects, as well as contribute to the
general body of knowledge about Software Engineering.

As a participant of one of these two rewrites, it would be very helpful
if you could answer some questions about. If you have participated in
both rewrites, please provide separate answers for each project.

Questions about the rewrite(s) you participated in
--------------------------------------------------

#. What was the problem with the older code base? Why did the
   maintainers decide to rewrite them from scratch?
#. How well did you know the older code base? How well did you know the
   new code base after the rewrite?
#. Is the new code base better than the old one? In which aspects?
#. In comparison with the older code base, the new code base provides:

  a) the same features?
  b) less features?
  c) more features?

Questions about the research itself and its presentation
--------------------------------------------------------

#. Would you like to review the paper before having it submitted for
   publication?
#. Would you like to receive a copy of the finished paper?
#. Is there anything else you want to ask or discuss, or any additional
   information you want to provide?
